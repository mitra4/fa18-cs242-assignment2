
class MovieVertex(object):

    """
    Stores info about movie like name date and gross
    Hash Hash and Eq so it can be used as dict key
    """

    def __init__(self, gross=0, date=0, name="NA"):
        self.gross = gross
        self.date = date
        self.name = name

    def __hash__(self):
        return hash((self.gross, self.date, self.name))

    def __eq__(self, other):
        return (self.gross, self.date, self.name) == (other.gross, other.date, other.name)

    def getGross(self):
        return self.gross

    def getDate(self):
        return self.date

    def getName(self):
        return self.name

    def setGross(self, newGross):
        self.gross = newGross

    def setName(self, newName):
        self.name = newName

    def setDate(self, newDate):
        self.date=newDate


