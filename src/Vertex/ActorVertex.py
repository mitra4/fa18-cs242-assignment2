
class ActorVertex(object):
    """
    Stores An Actor with age and name
    Has Hash and Eq so it can be used as dict key
    """
    def __init__(self, age, name):
        self.age = age
        self.name = name

    def __hash__(self):
        return hash((self.age, self.name))

    def __eq__(self, other):
        return (self.age, self.name) == (other.age, other.name)

    def getAge(self):
        return self.age

    def getName(self):
        return self.name

    def setAge(self, newAge):
        self.age = newAge

    def setName(self, newName):
        self.name = newName


