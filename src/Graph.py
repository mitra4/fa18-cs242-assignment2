import json
from src.Vertex.MovieVertex import MovieVertex
from src.Vertex.ActorVertex import ActorVertex
import jsonpickle


class Graph(object):

    def __init__(self, actorsAndAgesFP, actorsAndMoviesFP, moviesAndActorsFP, moviesAndDateFP, moviesAndGrossFP):
        with open(actorsAndAgesFP, 'r') as fp:
            self.actorsAndAges = json.load(fp)

        with open(actorsAndMoviesFP, 'r') as fp:
            self.actorsAndMovies = json.load(fp)

        with open(moviesAndActorsFP, 'r') as fp:
            self.moviesAndActors = json.load(fp)

        with open(moviesAndDateFP, 'r') as fp:
            self.moviesAndDate = json.load(fp)

        with open(moviesAndGrossFP, 'r') as fp:
            self.moviesAndGross = json.load(fp)

        self.builtGraph = self.buildGraph()

    def getActorsAndAges(self):
        return self.actorsAndAges

    def setActorsAndAges(self, newActorsAndAges):
        for key, val in newActorsAndAges.items():
            self.actorsAndAges[key] = val

    def getActorsAndMovies(self):
        return self.actorsAndMovies

    def setActorsAndMovies(self, newActorsAndMovies):
        for key, val in newActorsAndMovies.items():
            self.actorsAndMovies[key] = val

    def getMoviesAndActors(self):
        return self.moviesAndActors

    def setMoviesAndActors(self, newMoviesAndActors):
        for key, val in newMoviesAndActors.items():
            self.moviesAndActors[key] = val

    def getMoviesAndDate(self):
        return self.moviesAndDate

    def setMoviesAndDate(self, newMoviesAndDate):
        for key, val in newMoviesAndDate.items():
            self.moviesAndDate[key] = val

    def getMoviesAndGross(self):
        return self.moviesAndGross

    def setMoviesAndGross(self, newMoviesAndGross):
        for key, val in newMoviesAndGross.items():
            self.moviesAndGross[key] = val


    def buildGraph(self):

        """
        Builds the nested dictionary
        Every Node is represented by a key, its neighbors are stored in a dict where key is neighbor node, val is edge weight
        :return: Set builtGraph equal to the final dict we have below
        """

        # Create all movie vertices
        movieVertexList = []
        for key, val in self.moviesAndGross.items():
            addVert = MovieVertex(val, self.moviesAndDate[key], key)
            movieVertexList.append(addVert)

        # Create all actor vertices
        actorVertexList = []
        for key, val in self.actorsAndAges.items():
            addVert = ActorVertex(val, key)
            actorVertexList.append(addVert)

        # Init Dict
        graphDict = {}

        for val in actorVertexList:
            graphDict[val] = {}

        for val in movieVertexList:

            actorList = self.moviesAndActors[val.getName()]

            graphDict[val] = {}
            total = 0
            # Get total age of all actors
            for actor in actorList:
                try:
                    toAdd = self.actorsAndAges[actor]
                    total += toAdd
                except:
                    continue

            for actor in actorList:
                try:

                    #Multiple is just their age
                    actorMultiple = self.actorsAndAges[actor]
                    inputVert = None
                    for part in actorVertexList:
                        if actor == part.getName():
                            inputVert = part
                            break

                    # Create nodes and links here
                    graphDict[val][inputVert] = (((val.getGross()) / (total)) * actorMultiple)
                    graphDict[inputVert][val] = ((val.getGross()) / (total)) * actorMultiple
                except:
                    continue


        return graphDict

    def getMovieGross(self, inputName):
        """

        :param inputName: Name of movie you want to find gross for
        :return: Float val containing gross of movie or None if movie DNE
        """
        try:
            for key, val in self.builtGraph.items():
                if key.getName() == inputName:
                    return key.getGross()
        except:
            return None

    def getActorsMovies(self, inputName):
        """
        Simple dict lookup of actor and then iterate thru their movies
        :param inputName: Name of Actor
        :return: A list of movies the actor has been in or empty list if actor not found
        """
        try:
            for key, val in self.builtGraph.items():
                if key.getName() == inputName:
                    retList = []
                    for keyTwo, valTwo in self.builtGraph[key].items():
                        retList.append(keyTwo.getName())

                    return retList
        except:
            return []

    def getMoviesActors(self, inputName):
        """
        Also a pretty simple dict lookup and traversal
        :param inputName: Name of Movie
        :return: List of actors in movie or empty list if movie not found
        """
        try:
            for key, val in self.builtGraph.items():
                if key.getName() == inputName:
                    retList = []
                    for keyTwo, valTwo in self.builtGraph[key].items():
                        retList.append(keyTwo.getName())
                    return retList
        except:
            return []

    def TopXActors(self, numWanted):
        """

        :param numWanted: How many of the top grossing actors you want
        :return: List of actors from highest to lowest grossing
        """

        # Go and get all totals made by an actor
        actorDict = {}
        for key, val in self.builtGraph.items():
            if isinstance(key, ActorVertex):
                actorDict[key.getName()] = 0
                for keyTwo, valTwo in self.builtGraph[key].items():
                    actorDict[key.getName()] += valTwo

        retList = []
        # Iterate thru dict, find highest number, and pop. Repeat till done
        while len(retList) < numWanted and len(actorDict) != 0:
            maxVal = -1
            removeKey = None
            for key, val in actorDict.items():
                if val > maxVal:
                    maxVal = val
                    removeKey = key

            retList.append(removeKey)
            actorDict.pop(removeKey)

        return retList

    def OldestXActors(self, numWanted):
        """

        :param numWanted: How many oldest actors you want
        :return: List of actors sorted oldest to youngest
        """

        # Get all the actors and their ages
        actorDict = {}
        for key, val in self.builtGraph.items():
            if isinstance(key, ActorVertex):
                actorDict[key.getName()] = key.getAge()

        retList = []
        # Iterate thru dict, find highest number, and pop. Repeat till done
        while len(retList) < numWanted and len(actorDict) != 0:
            maxVal = -1
            removeKey = None
            for key, val in actorDict.items():
                if val > maxVal:
                    maxVal = val
                    removeKey = key

            retList.append(removeKey)
            actorDict.pop(removeKey)

        return retList

    def getAllMoviesInYear(self, yearChosen):
        """

        :param yearChosen: What year you want to lookup
        :return: All movies that premiered that year
        """
        movieList = []
        for key, val in self.builtGraph.items():
            if isinstance(key, MovieVertex):
                if key.getDate() == yearChosen:
                    movieList.append(key.getName())

        return movieList

    def getAllActorsInYear(self, yearChosen):
        """
        :param yearChosen: What year you want to lookup
        :return: All movies that premiered that year
        """
        actorList = set()
        for key, val in self.builtGraph.items():
            if isinstance(key, MovieVertex):
                if key.getDate() == yearChosen:
                    for keyTwo, valTwo in self.builtGraph[key].items():
                        actorList.add(keyTwo.getName())

        return list(actorList)
    
    def getAllActorRelations(self):
        """
        Gets a dict of actors
        Key is actor val is set of actors worked with
        :return: return dict of actors + who they worked with
        """
        actorDict = {}
        for key, val in self.builtGraph.items():
            if isinstance(key, ActorVertex):
                actorName = key.getName()
                actorDict[actorName] = set()
                for movieKey, movieVal in self.builtGraph[key].items():
                    if movieKey in self.builtGraph:
                        for actorKey, actorVal in self.builtGraph[movieKey].items():
                            if actorKey.getName() != actorName:
                                actorDict[actorName].add(actorKey.getName())

        hubDict = {}
        for key, val in actorDict.items():
            hubDict[key] = len(val)

        return hubDict

    def getAverageMoneyAge(self):
        """
        get age ranges of actors and return average money made
        :return: returns dict of actor age range and avg Dollars per movie made
        """

        #Initialize age total dict
        ageDictTotal = {}
        ageDictTotal['<18'] = (0,0)
        ageDictTotal['18-24'] = (0,0)
        ageDictTotal['25-34'] = (0,0)
        ageDictTotal['35-44'] = (0,0)
        ageDictTotal['45-54'] = (0,0)
        ageDictTotal['55-64'] = (0,0)
        ageDictTotal['65-74'] = (0,0)
        ageDictTotal['75+'] = (0,0)

        for key, val in self.builtGraph.items():
            if isinstance(key, ActorVertex):
                if int(key.getAge()) < 18:
                    newVal = ageDictTotal['<18']
                    lstNewVal = list(newVal)

                    for movieKey, movieVal in self.builtGraph[key].items():
                        lstNewVal[0] += movieVal
                        lstNewVal[1] += 1

                    newVal = tuple(lstNewVal)
                    ageDictTotal['<18'] = newVal

                elif int(key.getAge()) >= 18 and int(key.getAge()) <= 24:
                    newVal = ageDictTotal['18-24']
                    lstNewVal = list(newVal)

                    for movieKey, movieVal in self.builtGraph[key].items():
                        lstNewVal[0] += movieVal
                        lstNewVal[1] += 1

                    newVal = tuple(lstNewVal)
                    ageDictTotal['18-24'] = newVal

                elif int(key.getAge()) >= 25 and int(key.getAge()) <= 34:
                    newVal = ageDictTotal['25-34']
                    lstNewVal = list(newVal)

                    for movieKey, movieVal in self.builtGraph[key].items():
                        lstNewVal[0] += movieVal
                        lstNewVal[1] += 1

                    newVal = tuple(lstNewVal)
                    ageDictTotal['25-34'] = newVal

                elif int(key.getAge()) >= 35 and int(key.getAge()) <= 44:
                    newVal = ageDictTotal['35-44']
                    lstNewVal = list(newVal)

                    for movieKey, movieVal in self.builtGraph[key].items():
                        lstNewVal[0] += movieVal
                        lstNewVal[1] += 1

                    newVal = tuple(lstNewVal)
                    ageDictTotal['35-44'] = newVal

                elif int(key.getAge()) >= 45 and int(key.getAge()) <= 54:
                    newVal = ageDictTotal['45-54']
                    lstNewVal = list(newVal)

                    for movieKey, movieVal in self.builtGraph[key].items():
                        lstNewVal[0] += movieVal
                        lstNewVal[1] += 1

                    newVal = tuple(lstNewVal)
                    ageDictTotal['45-54'] = newVal

                elif int(key.getAge()) >= 55 and int(key.getAge()) <= 64:
                    newVal = ageDictTotal['55-64']
                    lstNewVal = list(newVal)

                    for movieKey, movieVal in self.builtGraph[key].items():
                        lstNewVal[0] += movieVal
                        lstNewVal[1] += 1

                    newVal = tuple(lstNewVal)
                    ageDictTotal['55-64'] = newVal

                elif int(key.getAge()) >= 65 and int(key.getAge()) <= 74:
                    newVal = ageDictTotal['65-74']
                    lstNewVal = list(newVal)

                    for movieKey, movieVal in self.builtGraph[key].items():
                        lstNewVal[0] += movieVal
                        lstNewVal[1] += 1

                    newVal = tuple(lstNewVal)
                    ageDictTotal['65-74'] = newVal

                else:
                    newVal = ageDictTotal['75+']
                    lstNewVal = list(newVal)

                    for movieKey, movieVal in self.builtGraph[key].items():
                        lstNewVal[0] += movieVal
                        lstNewVal[1] += 1

                    newVal = tuple(lstNewVal)
                    ageDictTotal['75+'] = newVal


        averagePaymentDict = {}
        for key, val in ageDictTotal.items():
            amtList = list(val)
            average = float(amtList[0]) / float(amtList[1])
            averagePaymentDict[key] = abs(average)

        return averagePaymentDict


    def getComponentDicts(self):
        """
        Find all the dicts that make up the graph
        :return: All the dicts that make up the graph
        """
        actorsAndAges = {}
        actorsAndMovies = {}
        moviesAndActors = {}
        moviesAndDate = {}
        moviesAndGross = {}

        for key, val in self.builtGraph.items():
            if isinstance(key, ActorVertex):
                actorsAndAges[key.getName()] = key.getAge()
                movieList = []
                for movieKey, movieVal in self.builtGraph[key].items():
                    if isinstance(movieKey, MovieVertex):
                        movieList.append(movieKey.getName())
                actorsAndMovies[key.getName()] = movieList

            if isinstance(key, MovieVertex):
                moviesAndDate[key.getName()] = key.getDate()
                moviesAndGross[key.getName()] = key.getGross()
                actorList = []
                for actorKey, actorVal in self.builtGraph[key].items():
                    if isinstance(actorKey, ActorVertex):
                        actorList.append(actorKey.getName())
                moviesAndActors[key.getName()] = actorList

        return actorsAndAges, actorsAndMovies, moviesAndDate, moviesAndGross, moviesAndActors

    def dumpGraphDicts(self):
        """
        dump all dicts to json files
        :return:
        """
        with open('actorsAndAges.json', 'w') as fp:
            json.dump(self.actorsAndAges, fp)

        with open('moviesAndGross.json', 'w') as fp:
            json.dump(self.moviesAndGross, fp)

        with open('moviesAndDate.json', 'w') as fp:
            json.dump(self.moviesAndDate, fp)

        with open('actorsAndMovies.json', 'w') as fp:
            json.dump(self.actorsAndMovies, fp)

        with open('moviesAndActors.json', 'w') as fp:
            json.dump(self.moviesAndActors, fp)

        return




def main():
    graph = Graph('JSON/actorsAndAges.json', 'JSON/actorsAndMovies.json', 'JSON/moviesAndActors.json', 'JSON/moviesAndDate.json', 'JSON/moviesAndGross.json')
    # print(graph.builtGraph)
    # with open('builtGraph.json', 'r') as fp:
    #     graph = jsonpickle.decode(fp.read(), keys=True, classes=(Graph, MovieVertex, ActorVertex))
    # print(graph.builtGraph)
    # print(graph.getMovieGross('Star Wars: The Last Jedi'))
    # print (graph.getActorsMovies('Laura Dern'))
    # print (graph.getMoviesActors('Star Wars: The Last Jedi'))
    # print(graph.TopXActors(1))
    #print(graph.OldestXActors(10))
    # print (graph.getAllMoviesInYear(2008))
    # print(graph.getAllActorsInYear(2008))
    #
    # frozen = jsonpickle.encode(graph, keys=True, make_refs=False)
    #
    # with open('builtGraph.json', 'w') as fp:
    #     fp.write(frozen)
    print(graph.getActorsMovies("Anna Kendrick"))


if __name__ == '__main__':
    main()
