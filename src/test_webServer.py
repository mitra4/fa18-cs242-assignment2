from unittest import TestCase
import Webserver
import flask

import os
import tempfile

import pytest

class TestWebServer(TestCase):
    def test_GetRequestActor(self):
        self.app = Webserver.app.test_client()
        response = self.app.get('/actors?name={"Tim"}&age={"58"}')
        self.assertEqual(str(response), '<Response streamed [200 OK]>')

        response = self.app.get('/actors?name={"Timea"}&name={"Jany"}')

    def test_GetRequestSpecActor(self):
        self.app = Webserver.app.test_client()
        response = self.app.get('/actors/Bruce_Willis')
        self.assertEqual(str(response), '<Response streamed [200 OK]>')
        response = self.app.get('/actors/Jake_Peralta')

    def test_getRequestMovieAll(self):
        self.app = Webserver.app.test_client()
        response = self.app.get('/movies?name={"The Verdict"}')

        response = self.app.get('/movies?name={"The_Verdict"}')
        self.assertEqual(str(response), '<Response streamed [400 BAD REQUEST]>')

        response = self.app.get('/movies?year={"2014"}')


    def test_GetRequestSpecMovie(self):
        self.app = Webserver.app.test_client()
        response = self.app.get('/movies/The_Verdict')

        response = self.app.get('/movies/The_Verdict')
        self.assertEqual(str(response), '<Response streamed [200 OK]>')

    def test_PutRequestActors(self):
        self.app = Webserver.app.test_client()
        response = self.app.put('/actors/Bruce_Willis?{"age"="84"}')
        self.assertEqual(str(response), '<Response streamed [200 OK]>')

    def test_PutRequestMovies(self):
        self.app = Webserver.app.test_client()
        response = self.app.put('/movies/The_Verdict?date={"2017"}')
        self.assertEqual(str(response), '<Response streamed [200 OK]>')

    def test_PostRequestActors(self):
        self.app = Webserver.app.test_client()
        response = self.app.put('/actors?name={"Ali Ryan"}&age={"42"}&movies={"Random Movie 2"} ')
        response = self.app.put('/actors?name={"Ali Ryan"}&age={"42"}')

    def test_PostRequestMovies(self):
        self.app = Webserver.app.test_client()
        response = self.app.put('/movies?name={"Random Movie 4"}&date={"2017"}&box_office={"120000"}&actors={"Jack Ryan"}')

    def test_DeleteRequestActors(self):
        self.app = Webserver.app.test_client()
        response = self.app.delete('/actors/Jack_Ryan')

    def test_DeleteRequestMovies(self):
        self.app = Webserver.app.test_client()
        response = self.app.delete('/movies/Random_Movies_2')
