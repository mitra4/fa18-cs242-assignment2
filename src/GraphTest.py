import unittest
import Graph


class MyTestCase(unittest.TestCase):

    graph = Graph.Graph()
    graph.buildGraph()

    def test_buildGraph(self):
        self.assertNotEqual(self.graph.builtGraph, None)

    def test_getActorMovies(self):
        self.assertEqual(self.graph.getActorsMovies('Laura Dern'), ['Star Wars: The Last Jedi'])
        self.assertEqual(self.graph.getActorsMovies('Bla'), None)

    def test_moviesGross(self):
        self.assertEqual(self.graph.getMovieGross('Star Wars: The Last Jedi'), 1333000000.0)
        self.assertEqual(self.graph.getMovieGross('bla'), None)

    def test_getMoviesActors(self):
        self.assertEqual(self.graph.getMoviesActors('Star Wars: The Last Jedi'), ['Adam Driver', 'Daisy Ridley', 'John Boyega', 'Oscar Isaac', "Lupita Nyong'o", 'Domhnall Gleeson', 'Anthony Daniels', 'Gwendoline Christie', 'Laura Dern', 'Benicio del Toro'])
        self.assertEqual(self.graph.getMoviesActors('bla'), None)

    def test_TopXActors(self):
        self.assertEqual(self.graph.TopXActors(1), ['John Francis Daley'])
        self.assertEqual(self.graph.TopXActors(0), [])

    def test_OldestActors(self):
        self.assertEqual(self.graph.OldestXActors(1), ['June Squibb'])
        self.assertEqual(self.graph.OldestXActors(0), [])

    def test_MoviesInYear(self):
        self.assertEqual(self.graph.getAllMoviesInYear(2008), ['Forgetting Sarah Marshall', 'The Hurt Locker'])

    def test_ActorsInYear(self):
        self.assertEqual(set(self.graph.getAllActorsInYear(2008)), set(['Anthony Mackie', 'Guy Pearce', 'Ralph Fiennes', 'Brian Geraghty', 'Jason Segel', 'Jeremy Renner', 'Evangeline Lilly', 'Kristen Bell']))


if __name__ == '__main__':
    unittest.main()
