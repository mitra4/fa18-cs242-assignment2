import bs4
import requests
import lxml
import re
import logging
import time
import json

class Webscraper:

    movieLinks = []
    actorLinks = []
    actorsAndAges = {}
    moviesAndGross = {}
    #actorLinks.append('https://en.wikipedia.org/wiki/Bridgit_Mendler')
    #actorLinks.append('https://en.wikipedia.org/wiki/David_Spade')
    actorLinks.append('https://en.wikipedia.org/wiki/Joseph_Gordon-Levitt')
    actorsAndMovies = {}
    moviesAndActors = {}
    moviesAndDates = {}

    def getActorInfo(self, startURL):
        """

        :param startURL: URL you are checking
        :return: Updates all the dictionaries and lists so we can keep searching.
        """

        startURL = startURL+"#Filmography"
        print(startURL)

        # Get URL HTML
        request = ''
        iterator = 0
        while request == '':
            iterator += 1
            if iterator == 6:
                return
            try:
                request = requests.get(startURL)
                break
            except:
                logging.debug("We are stopped by overload")
                time.sleep(0.5)
                continue
        soup = bs4.BeautifulSoup(request.content,'lxml')

        # Get Actor Name and add to dictionary
        name = soup.title.string
        sep = " - W"
        realName = name.split(sep, 1)[0]

        # Get actor age and add to Dict
        age = soup.find('span',  {'class' : 'noprint ForceAgeToShow'})
        if age == None:
            logging.warning("Actor is likely dead so page setup differently")
            return
        stringAge = (age.get_text())
        intAge = stringAge.split("age", 1)[1]
        intAge = int(intAge[1:-1])

        # Actor filmography always first table
        filmography = soup.findAll('table', {'class': ['wikitable sortable' , 'wikitable sortable plainrowheaders']})
        if len(filmography) == 0:
            logging.warning(realName + " not an actor")
            return

        self.actorsAndMovies[realName] = []
        filmography = filmography[0]

        # Go thru and pull out all film links
        for row in filmography.find_all("tr")[1:]:
            col = row.find_all("td")
            if (len(col) < 2):
                logging.warning(realName + " Setup of Wikipage not like other major actors")
                self.actorsAndMovies.pop(realName, None)
                return
            if len(col[0]) != 1:
                filmVals = col[0]
            else:
                filmVals = col[1]
            # Find all titles and links
            titles = filmVals.find_all('a', href=True)
            for val in titles:
                # Get specific link
                link = (val['href'])
                if link[0] != "#" and 'upcoming' not in link and "(" not in link:
                    full_link = 'https://en.wikipedia.org' + link
                    self.movieLinks.append(full_link)

                # Get specific title
                try:
                    title = val['title']
                    if 'upcoming' not in title:
                        self.actorsAndMovies[realName].append(title)
                except:
                    logging.warning("Cannot access titles on this page")
                    self.actorsAndMovies.pop(realName, None)

        self.actorsAndAges[realName] = intAge

    def getMovieInfo(self, startURL):
        print(startURL)
        # Get URL HTML
        request = ''
        iterator = 0
        while request == '':
            iterator += 1
            if iterator == 6:
                return
            try:
                request = requests.get(startURL)
                break
            except:
                logging.debug("Stopped by site")
                time.sleep(0.5)
                continue

        soup = bs4.BeautifulSoup(request.content, 'lxml')

        # Get movie title
        name = (soup.title.string)
        sep = " - W"
        realName = name.split(sep, 1)[0]

        # Get movie gross
        BO = soup.find_all(text="Box office")
        box = 0
        for office in BO:
            fullText = (office.parent.parent)
            box = (fullText.find_all(text=re.compile(r'\d+(?:,\d*)?')))
            if(len(box) == 0):
                return
            else:
                box = box[0]
            # Find spec value
            box = (re.search("\d+\.\d+", box))
            if box == None:
                logging.error("Box office in weird format")
                return
            box = float(box[0])
            multiple = fullText.find_all(text="billion")
            # Check whether to multiply by bill or mill
            if len(multiple) == 0:
                box *= 1000000
            else:
                box *= 1000000000

            break

        # Get Movie release year:
        years = soup.find_all(text="Release date")
        date = None
        for year in years:
            fullYear = year.parent.parent.parent
            dates = (fullYear.find_all(text=re.compile(r'\d+(?:,\d*)?')))
            if (len(dates) == 0):
                return
            else:
                date = dates[0]

            date = date[-4:]
            try:
                date = int(date)
            except:
                logging.error("Date Non-Standard returning")
                return
            break

        # Get actors on movie
        listOfActors = soup.find_all('div', {'class','plainlist'})
        if(len(listOfActors) < 3):
            logging.info("Wikipage for movie " + realName + " DNE")
            return

        actorFullInfo = listOfActors[1]

        if date == None:
            return

        self.moviesAndDates[realName] = date
        self.moviesAndGross[realName] = box

        self.moviesAndActors[realName] = []

        # Get all actors pushed into respective lists and Dicts
        actors = actorFullInfo.find_all("a", href=True)
        for val in actors:
            link = (val['href'])
            if link[0] != "#" and 'upcoming' not in link and "(" not in link:
                sep = '('
                rest = link.split(sep, 1)[0]
                full_link = 'https://en.wikipedia.org' + rest
                self.actorLinks.append(full_link)

            # Get specific title
            try:
                name = val['title']
                if 'upcoming' not in name:
                    self.moviesAndActors[realName].append(name)
            except:
                self.moviesAndDates.pop(realName, None)
                self.moviesAndGross.pop(realName, None)
                logging.warning("Cannot access actors on this page")
                return

        if self.moviesAndActors[realName] == []:
            actorFullInfo = listOfActors[2]

            # Get all actors pushed into respective lists and Dicts
            actors = actorFullInfo.find_all("a", href=True)
            for val in actors:
                link = (val['href'])
                if link[0] != "#" and 'upcoming' not in link and "(" not in link:
                    sep = '('
                    rest = link.split(sep, 1)[0]
                    full_link = 'https://en.wikipedia.org' + rest
                    self.actorLinks.append(full_link)

                # Get specific title
                try:
                    name = val['title']
                    if 'upcoming' not in name:
                        self.moviesAndActors[realName].append(name)
                except:
                    self.moviesAndDates.pop(realName, None)
                    self.moviesAndGross.pop(realName, None)
                    logging.warning("Cannot access actors on this page")
                    return




def main():
    """

    :return: Save all dicts as JSON elements to be used in graph
    """
    # Iterate thru links until you get enough links
    scraper = Webscraper()
    seenActors = []
    seenMovies = []
    t0 = time.time()
    iterator = 0
    # While links aren't sufficient keep getting them
    while(len(scraper.actorsAndAges) < 125 or len(scraper.moviesAndGross) < 60):
        time.sleep(0.1)
        if len(scraper.actorLinks) > 0:
            currentActor = scraper.actorLinks.pop()
            if len(scraper.actorsAndAges) < 250 or len(scraper.movieLinks) < 20:
                if currentActor not in seenActors:
                    scraper.getActorInfo(currentActor)
                seenActors.append(currentActor)

        if len(scraper.movieLinks) > 0:
            currentMovie = scraper.movieLinks.pop()
            if len(scraper.moviesAndGross) < 200 or len(scraper.actorLinks) < 20:
                if currentMovie not in seenMovies:
                    scraper.getMovieInfo(currentMovie)
                seenMovies.append(currentMovie)

        scraper.actorLinks = list(set(scraper.actorLinks))
        scraper.movieLinks = list(set(scraper.movieLinks))

        if(iterator % 50) == 0 and iterator != 0:
            logging.debug("Sleep so we don't overload Site")
            time.sleep(5)

        iterator += 1

        print(len(scraper.actorsAndAges))
        print(len(scraper.moviesAndGross))

    t1 = time.time()
    print(t1 - t0)
    # with open('actorsAndAges.json', 'w') as fp:
    #     json.dump(scraper.actorsAndAges, fp)
    #
    # with open('moviesAndGross.json', 'w') as fp:
    #     json.dump(scraper.moviesAndGross, fp)
    #
    # with open('moviesAndDate.json', 'w') as fp:
    #     json.dump(scraper.moviesAndDates, fp)
    #
    # with open('actorsAndMovies.json', 'w') as fp:
    #     json.dump(scraper.actorsAndMovies, fp)
    #
    # with open('moviesAndActors.json', 'w') as fp:
    #     json.dump(scraper.moviesAndActors, fp)

    return scraper

if __name__ == '__main__':
    main()

