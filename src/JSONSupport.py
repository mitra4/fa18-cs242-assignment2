import json
from Graph import Graph
import matplotlib.pyplot as plt


def getDicts(filename):
    actorsAndMovies = {}
    moviesAndActors = {}
    moviesAndDates = {}
    actorsAndAges = {}
    moviesAndGross = {}

    with open('data.json') as data_file:
        data = json.load(data_file)

    iterator = 0
    for obj in data:
        if iterator == 0:
            for actor in obj:
                actorsAndAges[data[0][actor]["name"]] = data[0][actor]["age"]
                actorsAndMovies[data[0][actor]["name"]] = data[0][actor]["movies"]

        else:
            for movie in obj:
                if data[1][movie]["year"] == 0 or data[1][movie]["box_office"] == 0 or len(data[1][movie]["actors"]) == 0:
                    continue

                moviesAndActors[data[1][movie]["name"]] = data[1][movie]["actors"]
                moviesAndDates[data[1][movie]["name"]] = data[1][movie]["year"]

                boxOffice = data[1][movie]["box_office"]
                if boxOffice < 2.5:
                    boxOffice = boxOffice * 1000000000
                elif boxOffice < 1000:
                    boxOffice = boxOffice * 1000000

                moviesAndGross[data[1][movie]["name"]] = boxOffice

        iterator += 1

    with open('actorsAndAges.json', 'w') as fp:
        json.dump(actorsAndAges, fp)

    with open('moviesAndGross.json', 'w') as fp:
        json.dump(moviesAndGross, fp)

    with open('moviesAndDate.json', 'w') as fp:
        json.dump(moviesAndDates, fp)

    with open('actorsAndMovies.json', 'w') as fp:
        json.dump(actorsAndMovies, fp)

    with open('moviesAndActors.json', 'w') as fp:
        json.dump(moviesAndActors, fp)

def main():
    #getDicts("data.json")
    graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json', 'moviesAndGross.json')
    plt.figure(figsize=(80,40))
    actorRelations = (graph.getAllActorRelations())

    for key, val in actorRelations.items():
        plt.bar(key, val)

    plt.legend(actorRelations.keys(),  prop={'size': 4})
    plt.savefig("actorRelations.png")

    # avgMoney = (graph.getAverageMoneyAge())
    # print(avgMoney)
    # for key, val in avgMoney.items():
    #     plt.bar(key, val)

    # plt.legend(prop={'size': 6})
    # plt.savefig("avgMoney.png")


if __name__ == '__main__':
    main()