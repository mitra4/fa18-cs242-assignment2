from flask import Flask, request, abort, jsonify, Response
from Graph import Graph
import JSONSupport
from src.Vertex.MovieVertex import MovieVertex
from src.Vertex.ActorVertex import ActorVertex

app = Flask(__name__)

class WebServer:

    @app.route("/actors", methods=['GET'])
    def GetRequestActor():
        """
        Handles Get request for boolean actors
        :return:
        """
        data = request.args
        argDict = {}
        iterator = 0

        #Get all args for the check
        for key, val in data.items():

            #Get First arg
            iterator += 1
            argDict[iterator] = {}
            firstInstance = val.find("\"")
            iterateWord = firstInstance + 1
            while val[iterateWord] != "\"":
                iterateWord += 1
            firstArg = val[firstInstance + 1 : iterateWord]
            if firstArg.isdigit():
                firstArg = int(firstArg)

            argDict[iterator][key] = firstArg
            restArgs = val[iterateWord+2:]
            restArgs = restArgs.split("|")
            #Get all or clauses in arg
            if len(restArgs) == 1:
                continue

            for i, arg in enumerate(restArgs):
                if arg == '':
                    continue

                startPos = arg.find("name")
                if startPos != -1:
                    indexPosition = startPos + 6
                    iterateWord = indexPosition + 1
                    while arg[iterateWord] != "\"":
                        iterateWord+=1

                    newArg = arg[indexPosition + 1: iterateWord]
                    argDict[iterator]["name"+str(i)] = newArg

                startPos = arg.find("age")
                if startPos != -1:
                    indexPosition = startPos + 5
                    iterateWord = indexPosition + 1
                    while arg[iterateWord] != "\"":
                        iterateWord += 1

                    newArg = arg[indexPosition + 1: iterateWord]
                    newArg = int(newArg)
                    argDict[iterator]["age"+str(i)] = newArg

        graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json', 'moviesAndGross.json')
        checkList = []

        #Pull all vals that meet args
        for argKey, argVal in argDict.items():
            argList = []
            for key, val in graph.builtGraph.items():
                if isinstance(key, ActorVertex):
                    for internalKey, internalVal in argDict[argKey].items():
                        if internalKey.find("name") != -1:
                            if internalVal in key.getName():
                                argList.append(key.getName())

                        elif internalKey.find("age") != -1:
                            if internalVal == key.getAge():
                                argList.append(key.getName())

            if len(argList) != 0:
                checkList.append(argList)

        if len(checkList) == 0:
            return abort(400, Response("No matching people"))
        if len(checkList) == 1:
            return jsonify(checkList)

        #Check all internal lists against each other and only pull all actors that are in all lists
        firstCheck = checkList[0]
        del checkList[0]
        for internalList in checkList:
            for val in firstCheck:
                if val not in internalList:
                    firstCheck.remove(val)

        return jsonify(firstCheck)

    @app.route('/actors/<path:path>')
    def GetRequestSpecActor(path=None):
        # Get Name of movie
        name = str(path)
        nameParts = name.split('_')
        actorName = ' '.join(word for word in nameParts)

        # Get Graph and all relevant details
        graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json', 'moviesAndGross.json')
        movies = graph.getActorsMovies(actorName)
        if movies is None or len(movies) == 0:
            return abort(400, Response("Has No Movies"))

        #Get all relevant info about actor
        age = 0
        totalGross = 0
        for key, val in graph.builtGraph.items():
            if isinstance(key, ActorVertex):
                if key.getName() == actorName:
                    age = key.getAge()
                    totalGross = 0
                    for keyTwo, valTwo in graph.builtGraph[key].items():
                        totalGross += valTwo

        if age == 0:
            return abort(400, Response("Age DNE"))

        #Dump to dict
        actorDict = {}
        actorDict[actorName] = {}
        actorDict[actorName]["Age"] = age
        actorDict[actorName]["Filmography"] = movies
        actorDict[actorName]["Amount Grossed"] = totalGross

        return jsonify(actorDict)




    @app.route("/movies", methods=["GET"])
    def getRequestMovieAll():
        """
        Get specific args for movies and return relevant movies
        :return:
        """
        data = request.args
        argDict = {}
        iterator = 0

        #Get all args, separated by ands
        for key, val in data.items():
            iterator += 1
            argDict[iterator] = {}
            firstInstance = val.find("\"")
            iterateWord = firstInstance + 1
            while val[iterateWord] != "\"":
                iterateWord += 1
            firstArg = val[firstInstance + 1: iterateWord]
            if firstArg.isdigit():
                firstArg = int(firstArg)

            argDict[iterator][key] = firstArg
            restArgs = val[iterateWord + 2:]

            #Check for any or clauses
            restArgs = restArgs.split("|")
            if len(restArgs) == 1:
                continue

            for i, arg in enumerate(restArgs):
                if arg == '':
                    continue

                startPos = arg.find("name")
                if startPos != -1:
                    indexPosition = startPos + 6
                    iterateWord = indexPosition + 1
                    while arg[iterateWord] != "\"":
                        iterateWord += 1

                    newArg = arg[indexPosition + 1: iterateWord]
                    argDict[iterator]["name" + str(i)] = newArg

                startPos = arg.find("year")
                if startPos != -1:
                    indexPosition = startPos + 5
                    iterateWord = indexPosition + 1
                    while arg[iterateWord] != "\"":
                        iterateWord += 1

                    newArg = arg[indexPosition + 1: iterateWord]
                    newArg = int(newArg)
                    argDict[iterator]["year" + str(i)] = newArg

                startPos = arg.find("box_office")
                if startPos != -1:
                    indexPosition = startPos + 12
                    iterateWord = indexPosition + 1
                    while arg[iterateWord] != "\"":
                        iterateWord += 1

                    newArg = arg[indexPosition + 1: iterateWord]
                    newArg = int(newArg)
                    argDict[iterator]["box_office" + str(i)] = newArg

        graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json', 'moviesAndGross.json')
        checkList = []

        #Find all args that match one of the clauses
        for argKey, argVal in argDict.items():
            argList = []
            for key, val in graph.builtGraph.items():
                if isinstance(key, MovieVertex):
                    for internalKey, internalVal in argDict[argKey].items():
                        if internalKey.find("name") != -1:
                            if internalVal in key.getName():
                                argList.append(key.getName())

                        elif internalKey.find("year") != -1:
                            if internalVal == key.getDate():
                                argList.append(key.getName())

                        elif internalKey.find("box_office") != 1:
                            if internalVal == key.getGross():
                                argList.append(key.getName())

            if len(argList) != 0:
                checkList.append(argList)

        if len(checkList) == 0:
            return abort(400,  Response("No actors found that match"))
        if len(checkList) == 1:
            return jsonify(checkList)

        #Find movies that exist in all internal lists
        firstCheck = checkList[0]
        del checkList[0]
        for internalList in checkList:
            for val in firstCheck:
                if val not in internalList:
                    firstCheck.remove(val)

        return jsonify(firstCheck)

    @app.route('/movies/<path:path>')
    def GetRequestSpecMovie(path=None):
        #Get Name of movie
        name = str(path)
        nameParts = name.split('_')
        movieName = ' '.join(word for word in nameParts)

        #Get Graph and all relevant details
        graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json', 'moviesAndGross.json')

        gross = graph.getMovieGross(movieName)
        if gross is None:
            return abort(400, Response("Gross DNE"))

        actors = graph.getMoviesActors(movieName)
        if len(actors) == 0:
            return abort(400, Response("No actors in this movie"))

        year = None
        for key, val in graph.builtGraph.items():
            if isinstance(key, MovieVertex):
                if key.getName() == movieName:
                    year = key.getDate()

        if year is None:
            return abort(400, Response("Year DNE"))

        #Convert to dict to return as JSON
        movieDict = {}
        movieDict[movieName] = {}
        movieDict[movieName]["Box Office"] = gross
        movieDict[movieName]["Year Released"] = year
        movieDict[movieName]["Cast"] = actors

        return jsonify(movieDict)


    @app.route("/actors/<path:path>", methods=['PUT'])
    def PutRequestActors(path=None):

        #Get Actor Name
        name = str(path)
        nameParts = name.split('_')
        actorName = ' '.join(word for word in nameParts)

        #Get All changed Values
        data = request.url
        data = str(data)
        startPos = data.find(path)
        internalArgs = data[startPos + len(path) + 1:]
        internalArgs = internalArgs.split(",")

        #Create All changed vals with key, val pairs
        argDict = {}
        iterator = 0
        for i, arg in enumerate(internalArgs):
            iterator+=1
            if arg == '':
                continue

            namePos = arg.find("name")
            if namePos != -1:
                indexPosition = namePos + 6
                iterateWord = indexPosition + 1
                while arg[iterateWord] != "\"":
                    iterateWord += 1

                newArg = arg[indexPosition + 1: iterateWord]
                argDict["name"] = newArg

            agePos = arg.find("age")
            if agePos != -1:
                indexPosition = agePos + 5
                iterateWord = indexPosition + 1
                while arg[iterateWord] != "\"":
                    iterateWord += 1

                newArg = arg[indexPosition + 1: iterateWord]
                newArg = int(newArg)
                argDict["age"] = newArg

            if namePos == -1 and agePos == -1:
                return abort(400, Response("trying to modify nonexistent category"))

        #Modify args
        graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json', 'moviesAndGross.json')
        for key, val in graph.builtGraph.items():
            if isinstance(key, ActorVertex):
                if key.getName() == actorName:
                    for argKey, argVal in argDict.items():
                        if argKey == 'name':
                            graph.actorsAndAges[argVal] = graph.actorsAndAges[actorName]
                            graph.actorsAndMovies[argVal] = graph.actorsAndMovies[actorName]
                            key.setName(argVal)
                        if argKey == 'age':
                            graph.actorsAndAges[actorName] = argVal
                            key.setAge(argVal)

        graph.dumpGraphDicts()
        return "200, sucessfully Modified"

    @app.route("/movies/<path:path>", methods=['PUT'])
    def PutRequestMovies(path=None):
        # Get Actor Name
        name = str(path)
        nameParts = name.split('_')
        movieName = ' '.join(word for word in nameParts)

        # Get All changed Values
        data = request.url
        data = str(data)
        startPos = data.find(path)
        internalArgs = data[startPos + len(path) + 1:]
        internalArgs = internalArgs.split(",")

        # Create All changed vals with key, val pairs
        argDict = {}
        iterator = 0
        for i, arg in enumerate(internalArgs):
            iterator += 1
            if arg == '':
                continue

            namePos = arg.find("name")
            if namePos != -1:
                indexPosition = namePos + 6
                iterateWord = indexPosition + 1
                while arg[iterateWord] != "\"":
                    iterateWord += 1

                newArg = arg[indexPosition + 1: iterateWord]
                argDict["name"] = newArg

            datePos = arg.find("date")
            if datePos != -1:
                indexPosition = datePos + 6
                iterateWord = indexPosition + 1
                while arg[iterateWord] != "\"":
                    iterateWord += 1

                newArg = arg[indexPosition + 1: iterateWord]
                newArg = int(newArg)
                print(newArg)
                argDict["date"] = newArg

            officePos = arg.find("box_office")
            if officePos != -1:
                indexPosition = officePos + 12
                iterateWord = indexPosition + 1
                while arg[iterateWord] != "\"":
                    iterateWord += 1

                newArg = arg[indexPosition + 1: iterateWord]
                newArg = int(newArg)
                argDict[iterator]["box_office"] = newArg

            if namePos == -1 and datePos == -1 and officePos == -1:
                return abort(400, Response("trying to modify nonexistent category"))

        graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json',
                      'moviesAndGross.json')

        #Modify args
        for key, val in graph.builtGraph.items():
            if isinstance(key, MovieVertex):
                if key.getName() == movieName:
                    for argKey, argVal in argDict.items():
                        if argKey == 'name':
                            key.setName(argVal)
                        if argKey == 'date':
                            key.setDate(argVal)
                            graph.moviesAndDate[movieName] = argVal
                        if argKey == "box_office":
                            graph.moviesAndGross[movieName] = argVal
                            key.setGross(argVal)

        graph.dumpGraphDicts()
        return "200, was successful"


    @app.route("/actors", methods=['POST'])
    def PostRequestActors():
        data = request.args
        name = None
        age = None
        movies = []
        #Get all relevant info avout actors
        for key, val in data.items():
            if key == "name":
                indexPosition = val.find("\"")
                iterateWord = indexPosition + 1
                while val[iterateWord] != "\"":
                    iterateWord += 1

                name = val[indexPosition + 1: iterateWord]

            if key == "age":
                indexPosition = val.find("\"")
                iterateWord = indexPosition + 1
                while val[iterateWord] != "\"":
                    iterateWord += 1

                age = val[indexPosition + 1: iterateWord]
                age = int(age)

            if key == "movies":
                movieList = val.split(",")
                for movie in movieList:
                    indexPosition = movie.find("\"")
                    iterateWord = indexPosition + 1
                    while movie[iterateWord] != "\"":
                        iterateWord += 1

                    movieName = movie[indexPosition + 1: iterateWord]
                    movies.append(movieName)

        graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json',
                      'moviesAndGross.json')

        actorsAndAges = graph.getActorsAndAges()
        if name in actorsAndAges:
            return abort(400, Response("already have this actor"))
        else:
            actorsAndAges[name] = age

        actorsAndMovies = graph.getActorsAndMovies()
        if name in actorsAndMovies:
            return abort(400, Response("already have this actor"))
        else:
            actorsAndMovies[name] = movies

        #push into JSON and dump
        graph.setActorsAndAges(actorsAndAges)
        graph.setActorsAndMovies(actorsAndMovies)

        graph.dumpGraphDicts()

        return "201, successfully created Actor"

    @app.route("/movies", methods=['POST'])
    def PostRequestMovies():
        data = request.args
        name = None
        date = None
        boxOffice = None
        actors = []

        #Get all relevant info for movies
        for key, val in data.items():
            if key == "name":
                indexPosition = val.find("\"")
                iterateWord = indexPosition + 1
                while val[iterateWord] != "\"":
                    iterateWord += 1

                name = val[indexPosition + 1: iterateWord]

            if key == "date":
                indexPosition = val.find("\"")
                iterateWord = indexPosition + 1
                while val[iterateWord] != "\"":
                    iterateWord += 1

                stringDate = val[indexPosition + 1: iterateWord]
                date = int(stringDate)
            if key == "box_office":
                indexPosition = val.find("\"")
                iterateWord = indexPosition + 1
                while val[iterateWord] != "\"":
                    iterateWord += 1

                stringBO = val[indexPosition + 1: iterateWord]
                boxOffice = int(stringBO)
            if key == "actors":
                actorList = val.split(",")
                for actor in actorList:
                    indexPosition = actor.find("\"")
                    iterateWord = indexPosition + 1
                    while actor[iterateWord] != "\"":
                        iterateWord += 1

                    actorName = actor[indexPosition + 1: iterateWord]
                    actors.append(actorName)


        graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json', 'moviesAndGross.json')

        moviesAndDates = graph.getMoviesAndDate()
        if name in moviesAndDates:
            return abort(400, Response("already have this movie"))
        else:
            moviesAndDates[name] = date

        moviesAndGross = graph.getMoviesAndGross()
        if name in moviesAndGross:
            return abort(400, Response("already have this movie"))
        else:
            moviesAndGross[name] = boxOffice

        moviesAndActors = graph.getMoviesAndActors()
        if name in moviesAndActors:
            return abort(400, Response("already have this movie"))
        else:
            moviesAndActors[name] = actors

        #Dump into relevant movie JSON
        graph.setMoviesAndActors(moviesAndActors)
        graph.setMoviesAndDate(moviesAndDates)
        graph.setMoviesAndGross(moviesAndGross)

        graph.dumpGraphDicts()

        return "201, successfully create movie"


    @app.route("/actors/<path:path>", methods=['DELETE'])
    def DeleteRequestActors(path=None):
        # Get Actor Name
        name = str(path)
        nameParts = name.split('_')
        actorName = ' '.join(word for word in nameParts)

        graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json',
                      'moviesAndGross.json')
        actorsAndAges, actorsAndMovies, moviesAndDate, moviesAndGross, moviesAndActors = graph.getComponentDicts()
        #Find key and delete
        for key, val in graph.builtGraph.items():
            if isinstance(key, ActorVertex):
                if key.getName() == actorName:
                    graph.builtGraph.pop(key)

                    graph.actorsAndAges.pop(key.getName())
                    graph.actorsAndMovies.pop(key.getName())
                    graph.dumpGraphDicts()
                    return "200, successfully deleted actor"

        return abort(400, Response("could not find actor"))


    @app.route("/movies/<path:path>", methods=['DELETE'])
    def  DeleteRequestMovies(path=None):
        # Get Movie Name
        name = str(path)
        nameParts = name.split('_')
        movieName = ' '.join(word for word in nameParts)

        graph = Graph('actorsAndAges.json', 'actorsAndMovies.json', 'moviesAndActors.json', 'moviesAndDate.json', 'moviesAndGross.json')
        actorsAndAges, actorsAndMovies, moviesAndDate, moviesAndGross, moviesAndActors = graph.getComponentDicts()

        #Find key and delete
        for key, val in graph.builtGraph.items():
            if isinstance(key, MovieVertex):
                if key.getName() == movieName:
                    graph.builtGraph.pop(key)
                    graph.moviesAndDate.pop(key.getName())
                    graph.moviesAndGross.pop(key.getName())
                    graph.moviesAndActors.pop(key.getName())
                    graph.dumpGraphDicts()
                    return "200, successfully deleted movie"

        return abort(400, Response("Could not find movie"))


if __name__ == '__main__':
    app.run()
